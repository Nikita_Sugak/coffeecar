﻿using System;
using _7New.Model;

namespace _7New.Controller
{
    public class SizeAndCost
    {
        public static int count = 0;
        public static double cost = 0;
        public static void CalculateSize(CoffeeCar[] coffeeCar)
        {
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                count += coffeeCar[i].Weight;
                if (count > CoffeeCar.maxSize)
                {
                    Console.WriteLine("НЕ ВМЕЩАЕТСЯ");
                    count = CoffeeCar.maxSize;
                    break;
                }
            }
        }

        public static void CalculateCost(CoffeeCar[] coffeeCar)
        {
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                cost += coffeeCar[i].Weight * 0.001 * coffeeCar[i].Cost;
            }
        }
    }
}