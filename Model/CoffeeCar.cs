﻿namespace _7New.Model
{
    public class CoffeeCar
    {
        public const int MIN_COST = 0;
        public const int MIN_WEIGHT = 0;
        public const int MAX_WEIGHT = 10000;

        static public int maxSize = 56390;
        public string Name { get; set; }
        private float cost;
        private int weight;

        public CoffeeCar()
        {
            Name = string.Empty;
            cost = 100;
            weight = 0;
        }

        public CoffeeCar(string name, float cost, int weight)
        {
            Name = name;
            Cost = cost;
            Weight = weight; 
        }
        
        public CoffeeCar(CoffeeCar coffeeCar)
        {
            Name = coffeeCar.Name;
            Cost = coffeeCar.Cost;
            Weight = coffeeCar.Weight;
        }

        public int MaxSize
        {
            get { return maxSize; }
        }

        public float Cost
        {
            get { return cost; }
            set
            {
                if (value >= MIN_COST)
                {
                    cost = value; 
                } 
            }
        }

        public int Weight
        {
            get { return weight; }
            set
            {
                if (value >= MIN_WEIGHT && value <= MAX_WEIGHT)
                {
                    weight = value; 
                } 
            }
        }

        override
        public string ToString()
        {
            return $"{Name} {Weight} {Cost}\n";
        }
    }
}