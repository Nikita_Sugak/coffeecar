﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7New.Model
{
    public class CoffeeCarWithSugar : CoffeeCar
    {
        private int sugar;

        public CoffeeCarWithSugar()
        {
        }

        public CoffeeCarWithSugar(string name, float cost, int weight, int sugar) : base(name, cost, weight)
        {
            Sugar = sugar;
        }

        public CoffeeCarWithSugar(CoffeeCarWithSugar coffeeCarWithSugar) : base(coffeeCarWithSugar)
        {
        }

        public int Sugar
        {
            get { return sugar; }
            set
            {
                sugar = value;
                if (value > 0 && value <= 10)
                {
                    sugar = value;
                }
                else if (value <= 0)
                {
                    sugar = 1;
                }
                else
                {
                    sugar = 10;
                }
            }
        }

        override
        public string ToString()
        {
            return $"{Name} {Weight} {Cost} {sugar}\n";
        }
    }
}
