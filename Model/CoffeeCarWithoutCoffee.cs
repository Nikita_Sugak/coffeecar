﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7New.Model
{
    public class CoffeeCarWithoutCoffee : CoffeeCar
    {
        private bool coffee = false;

        public CoffeeCarWithoutCoffee() 
        {
        }

        public CoffeeCarWithoutCoffee(string name, float cost, int weight) : base(name, cost, weight)
        {
        }

        public CoffeeCarWithoutCoffee(CoffeeCarWithoutCoffee coffeeCarWithoutCoffee) : base(coffeeCarWithoutCoffee)
        {
        }

        override
        public string ToString()
        {
            return $"{Name} {Weight} {Cost} {coffee}\n";
        }
    }
}