﻿using _7New.View;
using _7New.Model;
using _7New.Controller;
using _7New.Util;
using System;

namespace _7New
{
    class Program
    {
        public static int COFFEE_NUMBER = 5;
        public static void Main(string[] args)
        {
            CoffeeCar[] group = CoffeeCreator.create(COFFEE_NUMBER);
            CoffeeInitializator.init(group);
            SizeAndCost.CalculateSize(group);
            SizeAndCost.CalculateCost(group);
            Printer.Print(group);

            group = CoffeeCreator.createWithoutCoffee(COFFEE_NUMBER);
            CoffeeInitializator.init(group);
            SizeAndCost.CalculateSize(group);
            SizeAndCost.CalculateCost(group);
            Printer.Print(group);

            group = CoffeeCreator.createWithSugar(COFFEE_NUMBER);
            CoffeeInitializator.init(group);
            SizeAndCost.CalculateSize(group);
            SizeAndCost.CalculateCost(group);
            Printer.Print(group);

            Printer.Print("\nWeight = " + SizeAndCost.count);
            Printer.Print("\nCost = " + SizeAndCost.cost);

            Console.ReadKey();
        }
    }
}