﻿using _7New.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7New.Util
{
    public class CoffeeCreator
    {
        public static CoffeeCar[] create(int size)
        {
            CoffeeCar[] coffeeCar = new CoffeeCar[size];
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                coffeeCar[i] = new CoffeeCar();
            }

            return coffeeCar;
        }

        public static CoffeeCarWithoutCoffee[] createWithoutCoffee(int size)
        {
            CoffeeCarWithoutCoffee[] coffeeCar = new CoffeeCarWithoutCoffee[size];
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                coffeeCar[i] = new CoffeeCarWithoutCoffee();
            }

            return coffeeCar;
        }

        public static CoffeeCarWithSugar[] createWithSugar(int size)
        {
            CoffeeCarWithSugar[] coffeeCar = new CoffeeCarWithSugar[size];
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                coffeeCar[i] = new CoffeeCarWithSugar();
            }

            return coffeeCar;
        }
    }
}
