﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _7New.Model;

namespace _7New.Util
{
    public class CoffeeInitializator
    {
        public static string[] names = { "Alex", "Anna", "Alexey", "Andrey", "Victor", "Peter",
                                         "Nikita", "Pavel", "Dima", "Denis", "Sergey", "Michael", 
                                         "Vlad", "Vladimir", "Kristina", "Olya", "Nastya", "Igor" };

        public static void init(CoffeeCar[] coffeeCar)
        {
            Random random = new Random();
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                coffeeCar[i].Name = names[random.Next(names.Length)];
                coffeeCar[i].Weight = random.Next(20000);
                coffeeCar[i].Cost = random.Next(500);
            }
        }

        public static void init(CoffeeCarWithoutCoffee[] coffeeCar)
        {
            Random random = new Random();
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                coffeeCar[i].Name = names[random.Next(names.Length)];
                coffeeCar[i].Weight = random.Next(20000);
                coffeeCar[i].Cost = random.Next(500);
            }
        }

        public static void init(CoffeeCarWithSugar[] coffeeCar)
        {
            Random random = new Random();
            for (int i = 0; i < coffeeCar.Length; i++)
            {
                coffeeCar[i].Name = names[random.Next(names.Length)];
                coffeeCar[i].Weight = random.Next(20000);
                coffeeCar[i].Cost = random.Next(500);
                coffeeCar[i].Sugar = random.Next(10);
            }
        }
    }
}
