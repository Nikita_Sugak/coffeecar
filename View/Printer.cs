﻿using System;
using _7New.Model;

namespace _7New.View
{
    public class Printer
    {
        public static void Print(string msg)
        {
            Console.Write(msg);
        }

        public static void Print(CoffeeCar[] coffeeCars)
        {
            for (int i = 0; i < coffeeCars.Length; i++)
            {
                Print(coffeeCars[i].ToString());
            }
        }
    }
}